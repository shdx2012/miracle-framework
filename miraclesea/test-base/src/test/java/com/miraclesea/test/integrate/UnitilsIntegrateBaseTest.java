package com.miraclesea.test.integrate;

import org.unitils.UnitilsJUnit4;
import org.unitils.spring.annotation.SpringApplicationContext;

@SpringApplicationContext("classpath*:config/spring/root/testContext.xml")
public abstract class UnitilsIntegrateBaseTest extends UnitilsJUnit4 {

}
