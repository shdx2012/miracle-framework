package com.miraclesea.framework;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.miraclesea.framework.webmvc.controller.ApiDataNotFoundErrorHandlerTest;
import com.miraclesea.framework.webmvc.controller.ApiValidateErrorHandlerTest;
import com.miraclesea.framework.webmvc.vo.FeedbacksTest;

@RunWith(Suite.class)
@SuiteClasses({
		FeedbacksTest.class, 
		ApiValidateErrorHandlerTest.class, 
		ApiDataNotFoundErrorHandlerTest.class
		})
public final class AllWebmvcTests { }
