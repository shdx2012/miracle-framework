package com.miraclesea.component.log;

import org.aspectj.lang.ProceedingJoinPoint;

import com.miraclesea.framework.exception.LogicException;
import com.miraclesea.framework.exception.UserException;

class CallTrace {
	
	private final ProceedingJoinPoint pjp;
	
	CallTrace(final ProceedingJoinPoint pjp) {
		this.pjp = pjp;
	}
	
	String getSignatureInfo() {
		return String.format("Call: %s", pjp.getSignature().toLongString());
	}
	
	String getArgumentsInfo() {
		Object[] args = pjp.getArgs();
		int argsLength = args.length;
		if (0 == argsLength) {
			return "There is not any parameter";
		}
		StringBuilder argsInfo = new StringBuilder();
		argsInfo.append("There are ");
		argsInfo.append(argsLength);
		argsInfo.append(" parameter(s), value(s) are: ");
		int count = 1;
		for (Object each : args) {
			argsInfo.append(count);
			argsInfo.append(" -> ");
			argsInfo.append(each);
			if (count <= argsLength - 1) {
				argsInfo.append(", ");
			}
			count++;
		}
		return argsInfo.toString();
	}
	
	String getReturnInfo(final Object returnValue) {
		return String.format("Execute completed, the return value is: %s", returnValue);
	}
	
	String getUserExceptionInfo(final UserException e) {
		return String.format("Got an user exception. Error code is: %s, argument(s) are: %s", e.getErrorCode(), e.getArguments());
	}
	
	String getLogicExceptionInfo(final LogicException e) {
		return String.format("Got an logic exception. It is: %s", e);
	}
	
}
