package com.miraclesea.component.log;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.miraclesea.framework.exception.LogicException;
import com.miraclesea.framework.exception.UserException;

@Component("testLog")
public final class TestLogImpl implements TestLog {
	
	@Override
	public void voidAndNoParameter() { }
	
	@Override
	public void voidAndOneParameter(final Integer para) { }
	
	@Override
	public List<String> hasReturnValueAndTwoParameter(final String para1, final Integer para2) {
		return Arrays.asList("A", "B", "C");
	}
	
	@Override
	public void voidAndNoParameterThrowUserException() {
		throw new UserException("Arg1", "Arg2") {
			
			private static final long serialVersionUID = -3354938239732369593L;
		};
	}
	
	@Override
	public void voidAndOneParameterThrowLogicException(final Long para) throws LogicException {
		throw new LogicException("Test for voidAndOneParameterThrowLogicException") {
			
			private static final long serialVersionUID = 271222043087892461L;
		};
	}
	
	@Override
	public String hasReturnValueAndTwoParameterThrowException(final Integer para1, final String para2) {
		throw new IllegalStateException("Test for hasReturnValueAndTwoParameterThrowException");
	}
	
}
