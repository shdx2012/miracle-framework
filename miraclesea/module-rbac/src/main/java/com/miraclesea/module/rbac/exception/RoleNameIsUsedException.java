package com.miraclesea.module.rbac.exception;

import com.miraclesea.framework.dao.exception.DataIntegrityViolationException;

public final class RoleNameIsUsedException extends DataIntegrityViolationException {
	
	private static final long serialVersionUID = -3011797031748307298L;

	public RoleNameIsUsedException() {
		super("name", "Unique.role.name");
	}
}
