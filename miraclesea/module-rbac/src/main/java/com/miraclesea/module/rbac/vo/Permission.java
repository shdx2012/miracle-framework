package com.miraclesea.module.rbac.vo;

public interface Permission {
	
	String toPermissionString();
}
