package com.miraclesea.module.rbac.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.annotation.Resource;

import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

import com.miraclesea.module.rbac.entity.Role;
import com.miraclesea.module.rbac.service.RoleService;
import com.miraclesea.module.rbac.test.RoleAsserter;
import com.miraclesea.module.rbac.test.RoleBuilder;
import com.miraclesea.test.layer.ApiControllerBaseTest;

public final class ApiRoleControllerTest extends ApiControllerBaseTest {
	
	@Resource
	@InjectMocks
	private ApiRoleController apiRoleController;
	
	@Resource
	@Mock
	private RoleService roleService;
	
	@Test
	public void findAll() throws Exception {
		when(roleService.findAll(new PageRequest(0, 10))).thenReturn(RoleBuilder.createPage(10, 0, 100, 2, 2));
		ResultActions actual = getMockMvc().perform(get("/api/roles"))
			.andExpect(status().isOk())
			.andExpect(content().contentType("application/json;charset=UTF-8"));
		RoleAsserter.assertPage(actual, 10, 0, 100, 2, 2);
		verify(roleService).findAll(new PageRequest(0, 10));
	}
	
	@Test
	public void load() throws Exception {
		when(roleService.load("1")).thenReturn(RoleBuilder.createRole("1", 4, 4));
		ResultActions actual = getMockMvc().perform(get("/api/role/1"))
			.andExpect(status().isOk())
			.andExpect(content().contentType("application/json;charset=UTF-8"));
		RoleAsserter.assertRole(actual, "1", 4, 4);
		verify(roleService).load("1");
	}
	
	@Test
	public void createFailureForEmptyName() throws Exception {
		Role role = new Role();
		getMockMvc().perform(post("/api/role").content(toJson(role)).contentType(MediaType.APPLICATION_JSON).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.feedbacks", hasSize(1)))
				.andExpect(jsonPath("$.feedbacks[0].code", is("NotNull.role.name")));
		verifyZeroInteractions(roleService);
	}
	
	@Test
	public void createFailureForNameTooShort() throws Exception {
		Role role = new Role();
		role.setName("1");
		getMockMvc().perform(post("/api/role").content(toJson(role)).contentType(MediaType.APPLICATION_JSON).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.feedbacks", hasSize(1)))
				.andExpect(jsonPath("$.feedbacks[0].code", is("Size.role.name")));
		verifyZeroInteractions(roleService);
	}
	
	@Test
	public void createFailureForNameTooLong() throws Exception {
		Role role = new Role();
		role.setName("ABCDEFGHIGKLMNOPQRSTUVWXYZABCDEFGHIGKLMNOPQRSTUVWXYZ");
		getMockMvc().perform(post("/api/role").content(toJson(role)).contentType(MediaType.APPLICATION_JSON).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.feedbacks", hasSize(1)))
				.andExpect(jsonPath("$.feedbacks[0].code", is("Size.role.name")));
		verifyZeroInteractions(roleService);
	}
	
	@Test
	public void createSuccess() throws Exception {
		Role role = new Role();
		role.setName("name");
		getMockMvc().perform(post("/api/role").content(toJson(role)).contentType(MediaType.APPLICATION_JSON).characterEncoding("UTF-8"))
				.andExpect(status().isOk());
		verify(roleService).create(argThat(new ArgumentMatcher<Role>() {

			@Override
			public boolean matches(final Object argument) {
				if (argument instanceof Role && "name".equals(((Role) argument).getName())) {
					return true;
				}
				return false;
			}
			
		}));
	}
	
	@Test
	public void updateFailureForNameTooShort() throws Exception {
		Role role = new Role();
		role.setName("1");
		getMockMvc().perform(put("/api/role/1").content(toJson(role)).contentType(MediaType.APPLICATION_JSON).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.feedbacks", hasSize(1)))
				.andExpect(jsonPath("$.feedbacks[0].code", is("Size.role.name")));
		verifyZeroInteractions(roleService);
	}
	
	@Test
	public void updateFailureForNameTooLong() throws Exception {
		Role role = new Role();
		role.setName("ABCDEFGHIGKLMNOPQRSTUVWXYZABCDEFGHIGKLMNOPQRSTUVWXYZ");
		getMockMvc().perform(put("/api/role/1").content(toJson(role)).contentType(MediaType.APPLICATION_JSON).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.feedbacks", hasSize(1)))
				.andExpect(jsonPath("$.feedbacks[0].code", is("Size.role.name")));
		verifyZeroInteractions(roleService);
	}
	
	@Test
	public void updateSuccess() throws Exception {
		Role role = new Role();
		role.setName("name123");
		getMockMvc().perform(put("/api/role/1").content(toJson(role)).contentType(MediaType.APPLICATION_JSON).characterEncoding("UTF-8"))
				.andExpect(status().isOk());
		verify(roleService).update(argThat(new ArgumentMatcher<Role>() {

			@Override
			public boolean matches(final Object argument) {
				if (argument instanceof Role && "name123".equals(((Role) argument).getName()) && "1".equals(((Role) argument).getId())) {
					return true;
				}
				return false;
			}
			
		}));
	}
	
	@Test
	public void deleteSuccess() throws Exception {
		getMockMvc().perform(delete("/api/role/1"))
			.andExpect(status().isOk());
		verify(roleService).delete("1");
	}
}
