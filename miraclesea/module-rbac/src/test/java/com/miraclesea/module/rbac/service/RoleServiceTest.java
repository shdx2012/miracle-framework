package com.miraclesea.module.rbac.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import javax.annotation.Resource;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.google.common.collect.Lists;
import com.miraclesea.framework.dao.exception.DataIntegrityViolationException;
import com.miraclesea.module.rbac.dao.RoleDao;
import com.miraclesea.module.rbac.entity.Role;
import com.miraclesea.test.layer.ServiceBaseTest;

public final class RoleServiceTest extends ServiceBaseTest {
	
	@Resource
	@InjectMocks
	private RoleService roleService;
	
	@Resource
	@Mock
	private RoleDao roleDao;
	
	@Test
	public void createFailureWithRoleNameUsed() throws Exception {
		Role role = createNewRole();
		when(roleDao.findByName("Role Name")).thenReturn(role);
		try {
			roleService.create(role);
		} catch (final DataIntegrityViolationException ex) {
			assertThat(ex.getField(), is("name"));
			assertThat(ex.getKey(), is("Unique.role.name"));
		}
		verify(roleDao).findByName("Role Name");
	}
	
	@Test
	public void createSuccess() throws DataIntegrityViolationException {
		Role role = createNewRole();
		when(roleDao.findByName("Role Name")).thenReturn(null);
		when(roleDao.save(role)).thenReturn(role);
		roleService.create(role);
		verify(roleDao).findByName("Role Name");
		verify(roleDao).save(role);
	}
	
	@Test
	public void updateFailureWithRoleNameUsed() {
		Role role = createExistRole();
		when(roleDao.findByName("Role Name", "1")).thenReturn(role);
		try {
			roleService.update(role);
		} catch (final DataIntegrityViolationException ex) {
			assertThat(ex.getField(), is("name"));
			assertThat(ex.getKey(), is("Unique.role.name"));
		}
		verify(roleDao).findByName("Role Name", "1");
	}
	
	@Test
	public void updateSuccess() throws DataIntegrityViolationException {
		Role role = createExistRole();
		when(roleDao.findByName("Role Name", "1")).thenReturn(null);
		when(roleDao.updateNotNull(role)).thenReturn(role);
		roleService.update(role);
		verify(roleDao).findByName("Role Name", "1");
		verify(roleDao).updateNotNull(role);
	}
	
	@Test
	public void delete() {
		roleService.delete("1");
		verify(roleDao).delete("1");
	}
	
	@Test
	public void load() {
		when(roleDao.findOne("1")).thenReturn(new Role());
		assertReflectionEquals(new Role(), roleService.load("1"));
		verify(roleDao).findOne("1");
	}
	
	@Test
	public void findAll() {
		when(roleDao.findAll(new PageRequest(1, 10))).thenReturn(new PageImpl<Role>(Lists.newArrayList(new Role())));
		assertReflectionEquals(new PageImpl<Role>(Lists.newArrayList(new Role())), roleService.findAll(new PageRequest(1, 10)));
		verify(roleDao).findAll(new PageRequest(1, 10));
	}
	
	@Test
	public void isUsedRoleNameForNewRole() {
		when(roleDao.findByName("Role Name")).thenReturn(createNewRole());
		assertTrue(roleService.isUsedRoleName(createNewRole()));
		verify(roleDao).findByName("Role Name");
	}
	
	@Test
	public void isUsedRoleNameForExistRole() {
		when(roleDao.findByName("Role Name", "1")).thenReturn(createExistRole());
		assertTrue(roleService.isUsedRoleName(createExistRole()));
		verify(roleDao).findByName("Role Name", "1");
	}
	
	@Test
	public void isNotUsedRoleNameForNewRole() {
		when(roleDao.findByName("Role Name")).thenReturn(null);
		assertFalse(roleService.isUsedRoleName(createNewRole()));
		verify(roleDao).findByName("Role Name");
	}
	
	@Test
	public void isNotUsedRoleNameForExistRole() {
		when(roleDao.findByName("Role Name", "1")).thenReturn(null);
		assertFalse(roleService.isUsedRoleName(createExistRole()));
		verify(roleDao).findByName("Role Name", "1");
	}
	
	private Role createNewRole() {
		Role result = new Role();
		result.setName("Role Name");
		return result;
	}
	
	private Role createExistRole() {
		Role result = new Role();
		result.setId("1");
		result.setName("Role Name");
		return result;
	}
}
