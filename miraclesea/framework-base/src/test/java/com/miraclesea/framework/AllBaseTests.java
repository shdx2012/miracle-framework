package com.miraclesea.framework;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.miraclesea.framework.collection.ListTransformerTest;
import com.miraclesea.framework.encoding.EncodingTest;
import com.miraclesea.framework.exception.LogicExceptionTest;
import com.miraclesea.framework.exception.SystemExceptionTest;
import com.miraclesea.framework.exception.UserExceptionTest;
import com.miraclesea.framework.lang.BaseObjectTest;
import com.miraclesea.framework.reflection.ReflectionUtilTest;

@RunWith(Suite.class)
@SuiteClasses({
	BaseObjectTest.class, 
	SystemExceptionTest.class, 
	LogicExceptionTest.class, 
	UserExceptionTest.class, 
	ReflectionUtilTest.class, 
	EncodingTest.class, 
	ListTransformerTest.class
})
public final class AllBaseTests { }
