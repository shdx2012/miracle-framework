package com.miraclesea.framework;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.miraclesea.framework.dao.BaseJpaRepositoryTest;
import com.miraclesea.framework.dao.exception.OptimisticLockingExceptionTest;

@RunWith(Suite.class)
@SuiteClasses({
	OptimisticLockingExceptionTest.class, 
	BaseJpaRepositoryTest.class
	}
)
public final class AllDaoTests { }
